export const createQueue = (initialSize = 16) => {
    let storage = [];
    let firstElement = 0;
    let size = 0;
    const clear = () => {
        const newStorage = [];
        while (newStorage.length < initialSize) {
            newStorage.push(undefined);
        }
        storage = newStorage;
        size = 0;
        firstElement = 0;
    };
    clear();
    const deleteFirst = () => {
        if (size === 0) {
            throw new Error("Queue is already empty, so first element cannot be removed");
        }
        const elementToReturn = storage[firstElement];
        storage[firstElement] = undefined;
        firstElement++;
        size--;
        if (firstElement >= storage.length) {
            firstElement = 0;
        }
        return elementToReturn;
    };
    const at = (idx) => {
        if (idx < 0 ||
            idx >= size) {
            throw new Error("Index is out of bounds");
        }
        const iidx = (idx + firstElement) % storage.length;
        return storage[iidx];
    };
    const expand = () => {
        const newStorage = [];
        for (let i = 0; i < size; i++) {
            newStorage[i] = at(i);
        }
        while (newStorage.length < size * 2) {
            newStorage.push(undefined);
        }
        storage = newStorage;
        firstElement = 0;
    };
    const push = (e) => {
        if (size === storage.length) {
            expand();
        }
        storage[firstElement + size] = e;
        size++;
    };
    const q = {
        getSize: () => size,
        push,
        deleteFirst,
        at,
        clear,
    };
    return q;
};
