export function delay(timeMs) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(undefined);
        }, timeMs);
    });
}
export function padStart(s, l, c) {
    let res = "" + s;
    while (res.length < l) {
        res = c + res;
    }
    return res;
}
export function padEnd(s, l, c) {
    let res = "" + s;
    while (res.length < l) {
        res += c;
    }
    return res;
}
export function dispersion(mes) {
    const sqAverage = mes.sumOfSquares / mes.samples.getSize();
    const average = mes.sum / mes.samples.getSize();
    return sqAverage - average * average;
}
export function sigma(mes) {
    return Math.sqrt(dispersion(mes));
}
export function cropSigma(mes, sigmaCount = 3) {
    const average = mes.sum / mes.samples.getSize();
    const s = sigma(mes);
    const low = average - sigmaCount * s;
    const high = average + sigmaCount * s;
    let sum = 0;
    const newArr = [];
    for (let i = 0; i < mes.samples.getSize(); i++) {
        const a = mes.samples.at(i);
        const res = a >= low && a <= high;
        if (res) {
            newArr.push(a);
            sum += a;
        }
    }
    return {
        samples: newArr,
        min: low,
        max: high,
        sum: sum,
    };
}
const ACCURACY_SIGNS = 2;
const ACCURACY = Math.pow(10, ACCURACY_SIGNS);
function addTrailingZeros(v) {
    let r = "" + v;
    const lastIndexOf = r.lastIndexOf(".");
    if (lastIndexOf === -1) {
        r += "." + padEnd("", ACCURACY_SIGNS, '0');
    }
    else {
        const digitsAfterDot = r.length - lastIndexOf - 1;
        for (let i = digitsAfterDot; i < ACCURACY_SIGNS; i++) {
            r += "0";
        }
        return r;
    }
    return r;
}
function roundTime(value, multiplyBy) {
    return addTrailingZeros(Math.round(multiplyBy * value * ACCURACY) / ACCURACY);
}
export function formatTime(value) {
    if (value < 0.001) {
        return roundTime(value, 1000000) + "×10⁻⁹s";
    }
    if (value < 1.0) {
        return roundTime(value, 1000) + "×10⁻⁶s";
    }
    return roundTime(value, 1) + "×10⁻³s";
}
