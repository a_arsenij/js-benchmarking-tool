export function getPlot(values, rowsNumber) {
    const min = values.reduce((a, b) => Math.min(a, b));
    const max = values.reduce((a, b) => Math.max(a, b));
    const columnsNormalized = values.map((value) => {
        return (value - min) / (max - min) * rowsNumber;
    });
    const lines = [];
    for (let lineIdx = 0; lineIdx < rowsNumber; lineIdx++) {
        let line = "";
        for (let c = 0; c < values.length; c++) {
            const v = columnsNormalized[c];
            columnsNormalized[c] -= 1;
            if (v >= 1) {
                line += "█";
            }
            else if (v > 0) {
                line += " ▁▂▃▄▅▆▇█"[Math.floor(v * 9)];
            }
            else {
                line += " ";
            }
        }
        lines.push(line);
    }
    lines.reverse();
    return lines;
}
