import { benchmark } from "./benchmark";
(async () => {
    const r1 = await benchmark({
        title: "sum array, including gc samples",
        generateInput: () => [...Array(512)].map(() => Math.random()),
        implementation: (data) => data.map(v => Math.round(v * 19)).reduce((a, b) => a + b),
        benchmarkConfig: {
            ignoreGCSamples: false
        }
    });
    const r2 = await benchmark({
        title: "sum array, ignoring gc samples",
        generateInput: () => [...Array(512)].map(() => Math.random()),
        implementation: (data) => data.map(v => Math.round(v * 19)).reduce((a, b) => a + b),
        benchmarkConfig: {
            ignoreGCSamples: true
        }
    });
    console.log(r1);
    console.log(r2);
})();
