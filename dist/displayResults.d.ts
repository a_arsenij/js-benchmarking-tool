import { Queue } from "./queue";
export declare type Measures = {
    sum: number;
    sumOfSquares: number;
    min: number;
    max: number;
    ops: number;
    samples: Queue<number>;
};
export declare type DisplayResultsArg = {
    title: string;
    measures: Measures;
    lastDisplayedAt: [number];
    measuredIterations?: number;
    iterationNumber?: number;
    batchSize?: number;
    plotSigmas?: number;
    plotColumns?: number;
    plotRows?: number;
    displayOncePerMs?: number;
    isPotentiallyLast?: boolean;
};
export declare function displayResults({ title, measures, lastDisplayedAt, measuredIterations, iterationNumber, batchSize, plotSigmas, plotColumns, plotRows, displayOncePerMs, isPotentiallyLast, }: DisplayResultsArg): string;
