import { Measures } from "./displayResults";
export declare type BenchmarkConfig = {
    batchSize?: number;
    warmupIterations?: number;
    measuredIterations?: number;
    delayBetweenIterations?: number;
    ignoreGCSamples?: boolean;
};
export declare type BenchmarkOutputConfig = {
    distributionPlotSigma?: number;
    plotColumns?: number;
    plotRows?: number;
    displayOncePerMs?: number;
};
export declare type Benchmark<INPUT, OUTPUT> = {
    title: string;
    generateInput: (batchInputIndex: number, iterationIndex: number) => INPUT;
    implementation: (input: INPUT, idx: number) => OUTPUT;
    benchmarkConfig?: BenchmarkConfig;
    outputConfig?: BenchmarkOutputConfig;
};
export declare function addMeasure(measures: Measures, value: number, batchSize: number): void;
export declare function trimMeasures(measures: Measures, size: number, batchSize: number): void;
export declare function createEmptyMeasures(): Measures;
export declare function benchmark<INPUT, OUTPUT>({ title, generateInput, implementation, benchmarkConfig, outputConfig }: Benchmark<INPUT, OUTPUT>): Promise<string>;
