import { Measures } from "./displayResults";
export declare function delay(timeMs: number): Promise<undefined>;
export declare function padStart(s: unknown, l: number, c: string): string;
export declare function padEnd(s: unknown, l: number, c: string): string;
export declare function dispersion(mes: Measures): number;
export declare function sigma(mes: Measures): number;
export declare function cropSigma(mes: Measures, sigmaCount?: number): {
    samples: number[];
    min: number;
    max: number;
    sum: number;
};
export declare function formatTime(value: number): string;
