export declare type Queue<T> = {
    deleteFirst: () => T;
    push: (elem: T) => void;
    clear: () => void;
    getSize: () => number;
    at: (index: number) => T;
};
export declare const createQueue: <T>(initialSize?: number) => Queue<T>;
