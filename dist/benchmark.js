import { delay } from "./utils";
import { displayResults } from "./displayResults";
import { createQueue } from "./queue";
export function addMeasure(measures, value, batchSize) {
    measures.samples.push(value);
    measures.max = Math.max(measures.max, value);
    measures.min = Math.min(measures.min, value);
    measures.sum += value;
    measures.sumOfSquares += value * value;
    measures.ops += batchSize;
}
export function trimMeasures(measures, size, batchSize) {
    while (measures.samples.getSize() > size) {
        measures.sum -= measures.samples.at(0);
        measures.sumOfSquares -= measures.samples.at(0) * measures.samples.at(0);
        measures.ops -= batchSize;
        measures.samples.deleteFirst();
    }
}
export function createEmptyMeasures() {
    return {
        sum: 0,
        sumOfSquares: 0,
        min: Number.POSITIVE_INFINITY,
        max: Number.NEGATIVE_INFINITY,
        ops: 0,
        samples: createQueue(),
    };
}
export async function benchmark({ title, generateInput, implementation, benchmarkConfig, outputConfig }) {
    const batchSize = (benchmarkConfig === null || benchmarkConfig === void 0 ? void 0 : benchmarkConfig.batchSize) || 8 * 1024;
    const warmupIterations = (benchmarkConfig === null || benchmarkConfig === void 0 ? void 0 : benchmarkConfig.warmupIterations) || 10;
    const measuredIterations = (benchmarkConfig === null || benchmarkConfig === void 0 ? void 0 : benchmarkConfig.measuredIterations) || 1024;
    const delayBetweenIterations = (benchmarkConfig === null || benchmarkConfig === void 0 ? void 0 : benchmarkConfig.delayBetweenIterations) || 4;
    const ignoreGCSamples = (benchmarkConfig === null || benchmarkConfig === void 0 ? void 0 : benchmarkConfig.ignoreGCSamples) || false;
    const distributionPlotSigma = (outputConfig === null || outputConfig === void 0 ? void 0 : outputConfig.distributionPlotSigma) || 3;
    const plotColumns = (outputConfig === null || outputConfig === void 0 ? void 0 : outputConfig.plotColumns) || 60;
    const plotRows = (outputConfig === null || outputConfig === void 0 ? void 0 : outputConfig.plotRows) || 8;
    const displayOncePerMs = (outputConfig === null || outputConfig === void 0 ? void 0 : outputConfig.displayOncePerMs) || 1000;
    async function runIteration(iterNumber) {
        var _a, _b;
        const data = [...Array(batchSize)].map((_, idx) => generateInput(idx, iterNumber));
        const results = [...Array(batchSize)];
        await delay(delayBetweenIterations);
        const startBytes = (_a = performance.memory) === null || _a === void 0 ? void 0 : _a.usedJSHeapSize;
        const startTime = performance.now();
        for (let i = 0; i < batchSize; i++) {
            results[i] = implementation(data[i], i);
        }
        const endTime = performance.now();
        const endBytes = (_b = performance.memory) === null || _b === void 0 ? void 0 : _b.usedJSHeapSize;
        if (ignoreGCSamples && endBytes < startBytes) {
            return undefined;
        }
        let counter = 0;
        results.forEach(data => {
            counter += ("" + data).length;
            if (counter < 0) {
                console.log("Side effect may happen, so JS will not DCE calling implementation's function");
            }
        });
        return endTime - startTime;
    }
    for (let i = 0; i < warmupIterations; i++) {
        console.log(`Running warmup iteration #${i + 1}/${warmupIterations}...`);
        await runIteration(i);
    }
    const measures = createEmptyMeasures();
    let result = "";
    const lastDisplayedAt = [0];
    for (let i = 0; i < measuredIterations; i++) {
        const value = await runIteration(i);
        if (value === undefined) {
            continue;
        }
        addMeasure(measures, value, batchSize);
        result = displayResults({
            title,
            measures,
            iterationNumber: i + 1,
            batchSize: batchSize,
            measuredIterations: measuredIterations,
            plotSigmas: distributionPlotSigma,
            plotColumns: plotColumns,
            plotRows: plotRows,
            displayOncePerMs: displayOncePerMs,
            lastDisplayedAt: lastDisplayedAt,
        });
    }
    return result;
}
