import { delay } from "./utils";
import { displayResults, Measures } from "./displayResults";
import {createQueue} from "./queue";

export type BenchmarkConfig = {
    batchSize?: number,
    warmupIterations?: number,
    measuredIterations?: number,
    delayBetweenIterations?: number,
    ignoreGCSamples?: boolean,
}

export type BenchmarkOutputConfig = {
    distributionPlotSigma?: number,
    plotColumns?: number,
    plotRows?: number,
    displayOncePerMs?: number,
}

export type Benchmark<INPUT, OUTPUT> = {
    title: string,
    generateInput: (
        batchInputIndex: number,
        iterationIndex: number,
    ) => INPUT,
    implementation: (input: INPUT, idx: number) => OUTPUT,
    benchmarkConfig?: BenchmarkConfig,
    outputConfig?: BenchmarkOutputConfig,
}

export function addMeasure(
    measures: Measures,
    value: number,
    batchSize: number,
) {
    measures.samples.push(value);
    measures.max = Math.max(measures.max, value);
    measures.min = Math.min(measures.min, value);
    measures.sum += value;
    measures.sumOfSquares += value * value;
    measures.ops += batchSize;
}

export function trimMeasures(
    measures: Measures,
    size: number,
    batchSize: number,
) {
    while (measures.samples.getSize() > size) {
        measures.sum -= measures.samples.at(0);
        measures.sumOfSquares -= measures.samples.at(0) * measures.samples.at(0);
        measures.ops -= batchSize;
        measures.samples.deleteFirst();
    }
}

export function createEmptyMeasures(): Measures {
    return {
        sum: 0,
        sumOfSquares: 0,
        min: Number.POSITIVE_INFINITY,
        max: Number.NEGATIVE_INFINITY,
        ops: 0,
        samples: createQueue<number>(),
    };
}

export async function benchmark<
    INPUT,
    OUTPUT,
>({
    title,
    generateInput,
    implementation,
    benchmarkConfig,
    outputConfig
}: Benchmark<INPUT, OUTPUT>) {

    const batchSize = benchmarkConfig?.batchSize || 8 * 1024;
    const warmupIterations = benchmarkConfig?.warmupIterations || 10;
    const measuredIterations = benchmarkConfig?.measuredIterations || 1024;
    const delayBetweenIterations = benchmarkConfig?.delayBetweenIterations || 4;
    const ignoreGCSamples = benchmarkConfig?.ignoreGCSamples || false;
    const distributionPlotSigma = outputConfig?.distributionPlotSigma || 3;
    const plotColumns = outputConfig?.plotColumns || 60;
    const plotRows = outputConfig?.plotRows || 8;
    const displayOncePerMs = outputConfig?.displayOncePerMs || 1000;

    async function runIteration(
        iterNumber: number,
    ): Promise<number | undefined> {
        const data = [...Array(batchSize)].map((_, idx) => generateInput(
            idx,
            iterNumber,
        ));
        const results = [...Array(batchSize)] as OUTPUT[];
        await delay(delayBetweenIterations);
        const startBytes = (performance as any).memory?.usedJSHeapSize;
        const startTime = performance.now();
        for (let i = 0; i < batchSize; i++) {
            results[i] = implementation(data[i], i);
        }
        const endTime = performance.now();
        const endBytes = (performance as any).memory?.usedJSHeapSize;
        if (ignoreGCSamples && endBytes < startBytes) {
            return undefined;
        }
        let counter = 0;
        results.forEach(data => {
                counter += (""+data).length;
                if (counter < 0) {
                    console.log("Side effect may happen, so JS will not DCE calling implementation's function")
                }
        });
        return endTime - startTime;
    }

    for (let i = 0; i < warmupIterations; i++) {
        console.log(`Running warmup iteration #${i+1}/${warmupIterations}...`);
        await runIteration(i);
    }

    const measures: Measures = createEmptyMeasures();

    let result: string = "";
    const lastDisplayedAt: [number] = [0];
    for (let i = 0; i < measuredIterations; i++) {


        const value = await runIteration(i);
        if (value === undefined) {
            continue;
        }
        addMeasure(
            measures,
            value,
            batchSize,
        );
        result = displayResults({
            title,
            measures,
            iterationNumber: i+1,
            batchSize: batchSize,
            measuredIterations: measuredIterations,
            plotSigmas: distributionPlotSigma,
            plotColumns: plotColumns,
            plotRows: plotRows,
            displayOncePerMs: displayOncePerMs,
            lastDisplayedAt: lastDisplayedAt,
        })

    }

    return result;



}
