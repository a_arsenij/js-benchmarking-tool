import { getPlot } from "./plot";
import { cropSigma, formatTime, padEnd, padStart } from "./utils";
import {Queue} from "./queue";

export type Measures = {
    sum: number,
    sumOfSquares: number,
    min: number,
    max: number,
    ops: number,
    samples: Queue<number>,
}

export type DisplayResultsArg = {
    title: string,
    measures: Measures,
    lastDisplayedAt: [number],
    measuredIterations?: number,
    iterationNumber?: number,
    batchSize?: number,
    plotSigmas?: number,
    plotColumns?: number,
    plotRows?: number,
    displayOncePerMs?: number,
    isPotentiallyLast?: boolean,
}

export function displayResults({
    title,
    measures,
    lastDisplayedAt,
    measuredIterations = measures.ops,
    iterationNumber = measures.ops,
    batchSize = 1,
    plotSigmas = 3,
    plotColumns = 60,
    plotRows = 8,
    displayOncePerMs = 1000,
    isPotentiallyLast = true,
}: DisplayResultsArg) {


    const filteredSamples = cropSigma(measures, plotSigmas);

    const vmin = Math.max(measures.min, filteredSamples.min);
    const vmax = Math.min(measures.max, filteredSamples.max);

    const d = vmax - vmin;

    const columns = [...Array(plotColumns)].map((_, idx) => {
        const from = vmin + d * (idx) / plotColumns;
        const to = vmin + d * (idx + 1) / plotColumns;
        return filteredSamples.samples.filter((v) => v >= from && v < to).length;
    });

    const min = formatTime( vmin / batchSize);
    const max = formatTime( vmax / batchSize);

    const allavg = formatTime(measures.sum / measures.ops);
    const allmin = formatTime(measures.min / batchSize);
    const allmax = formatTime(measures.max / batchSize);

    const sigavg = formatTime(filteredSamples.sum / measures.ops);
    const sigmin = formatTime(filteredSamples.min / batchSize);
    const sigmax = formatTime(filteredSamples.max / batchSize);

    function p(a: string, b: string) {
        return padStart(a, Math.max(a.length, b.length), ' ');
    }

    const result = `/*
 * ${title}
 * Iter: ${(iterationNumber)}/${measuredIterations}, Ops per sample: ${batchSize}, Total ops: ${measures.ops}
 * All: Avg: ${p(allavg, sigavg)}/op Min: ${p(allmin, sigmin)}/op, Max: ${p(allmax, sigmax)}/op
 * ${padStart(plotSigmas, 2, ' ')}σ: Avg: ${p(sigavg, allavg)}/op Min: ${p(sigmin, allmin)}/op, Max: ${p(sigmax, allmax)}/op
 *\n` +
        getPlot(
            columns,
            plotRows
        ).map(v => " * "+v).join("\n")+`
 * ${min}${padEnd("", plotColumns - min.length - max.length, " ")}${max}
 */
 
`;

    const now = new Date().getTime();
    if (
        (iterationNumber === measuredIterations && isPotentiallyLast) ||
        now - lastDisplayedAt[0] > displayOncePerMs
    ) {
        lastDisplayedAt[0] = now;
        console.clear();
        console.log(result);
    }

    return result;

}
