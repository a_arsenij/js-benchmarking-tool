export type Queue<T> = {

    deleteFirst: () => T,
    push: (elem: T) => void,
    clear: () => void,
    getSize: () => number,
    at: (index: number) => T,

}

export const createQueue = <T>(
    initialSize = 16,
) => {
    let storage: (T | undefined)[] = [];
    let firstElement = 0;
    let size = 0;

    const clear = () => {
        const newStorage: (T | undefined)[] = [];
        while (newStorage.length < initialSize) {
            newStorage.push(undefined);
        }
        storage = newStorage;
        size = 0;
        firstElement = 0;
    };
    clear();

    const deleteFirst = () => {
        if (size === 0) {
            throw new Error(
                "Queue is already empty, so first element cannot be removed",
            );
        }
        const elementToReturn = storage[firstElement];
        storage[firstElement] = undefined;
        firstElement++;
        size--;
        if (firstElement >= storage.length) {
            firstElement = 0;
        }
        return elementToReturn as T;
    };

    const at = (idx: number) => {
        if (
            idx < 0 ||
            idx >= size
        ) {
            throw new Error("Index is out of bounds");
        }
        const iidx = (idx + firstElement) % storage.length;
        return storage[iidx] as T;
    };

    const expand = () => {
        const newStorage: (T | undefined)[] = [];
        for (let i = 0; i < size; i++) {
            newStorage[i] = at(i);
        }
        while (newStorage.length < size * 2) {
            newStorage.push(undefined);
        }
        storage = newStorage;
        firstElement = 0;
    };

    const push = (e: T) => {
        if (size === storage.length) {
            expand();
        }
        storage[firstElement + size] = e;
        size++;
    };

    const q: Queue<T> = {
        getSize: () => size,
        push,
        deleteFirst,
        at,
        clear,
    };

    return q;
};
